import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private translateService: TranslateService) {
    platform.ready().then(() => {

      this.initTranslate();

      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  
  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translateService.setDefaultLang('en');

    if (this.translateService.getBrowserLang() !== undefined) {
        this.translateService.use(this.translateService.getBrowserLang());
    } else {
        this.translateService.use('en'); // Set your language here
    }
  }
}

