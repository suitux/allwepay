import { WhatHavePeopleToPayPage } from './../pages/what-have-people-to-pay/what-have-people-to-pay';
import { ItemPeopleRelationPage } from './../pages/item-people-relation/item-people-relation';
import { PeopleListComponent } from './../components/people-list/people-list';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SocialSharing } from '@ionic-native/social-sharing';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ItemListComponent } from '../components/item-list/item-list';
import { ItemsPage } from '../pages/items/items';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PeopleListComponent,
    ItemListComponent,
    ItemsPage,
    ItemPeopleRelationPage, 
    WhatHavePeopleToPayPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicModule, 
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PeopleListComponent,
    ItemListComponent,
    ItemsPage,
    ItemPeopleRelationPage, 
    WhatHavePeopleToPayPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}, 
    SocialSharing
  ]
})

export class AppModule {}
