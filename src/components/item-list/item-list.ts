import { Component } from '@angular/core';
import { Events } from 'ionic-angular';

@Component({
  selector: 'item-list',
  templateUrl: 'item-list.html'
})
export class ItemListComponent {

  items = [];

  constructor(public events: Events) {

  }

  addItem(name:string, price:number, quantity:number) {

    var itemOcurrences = this.getItemOcurrences(name);

    if(itemOcurrences == 0) {
      this.items.push({name, price, quantity});
      this.onAddItem({name, price, quantity});
    }
    else
      throw new Error("Item already exists");
  }

  getItemOcurrences(name: string) : number {
    var itemOcurrences : number = 0;

    this.items.forEach(item => {
      if(item.name == name)
        itemOcurrences = itemOcurrences + 1;
    });

    return itemOcurrences;
  }

  editItem(itemName : string, newItem) {

    var itemOcurrences = this.getItemOcurrences(newItem.name);
    var maxOcurrences : number = 1;

    if(itemName == newItem.name)
      maxOcurrences = 2;

    var oldItem;

    if(itemOcurrences < maxOcurrences) {
      this.items.forEach(item => {
        if(item.name == itemName) {
          oldItem = item;
          item.name = newItem.name;
          item.price = newItem.price;
          item.quantity = newItem.quantity;
          this.onEditItem(oldItem, item);
        }
      });
    }
    else {
      throw new Error("Item already exists");
    }
  }

  removeItem(name: string) {
    var indexToRemove : number = -1;

    this.items.forEach((item, index) => {
      if(item.name == name) {
        indexToRemove = index;
      }
    });

    if(indexToRemove != -1) {
      var itemRemoved = this.items[indexToRemove];
      this.items.splice(indexToRemove, 1);
      this.onRemoveItem(itemRemoved);
    }
  }
  
  onAddItem(item) {
    this.events.publish('item:added', item);
  }

  onEditItem(oldItem, newItem) {
    this.events.publish('item:edited', oldItem, newItem);
  }
   
  onRemoveItem(item) {
    this.events.publish('item:removed', item);
  }

  onItemClick(item) {
    this.events.publish('item:click', item);
  }

}