import { Component } from '@angular/core';

@Component({
  selector: 'people-list',
  templateUrl: 'people-list.html'
})
export class PeopleListComponent {

  private idCounter: number = 0;

  people = [];

  constructor() {

  }

  addPerson(name:string) {

    this.idCounter = this.idCounter + 1;

    var id: number = this.idCounter;

    this.people.push({id, name});
  }

  removePerson(person) {
    this.people.splice(this.people.indexOf(person), 1);
  }

  cleanList() {

    let peopleToDelete = [];

    this.people.forEach(person => {
      if(person.name.trim().length == 0)
        peopleToDelete.push(person);
    });

    peopleToDelete.forEach(personToDelete => {
      this.removePerson(personToDelete);
    });
  }

  isAValidList(): boolean {
    this.cleanList();
    return this.people.length > 0
  }

}
