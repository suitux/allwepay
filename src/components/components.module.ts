import { NgModule } from '@angular/core';
import { PeopleListComponent } from './people-list/people-list';
import { ItemListComponent } from './item-list/item-list';
@NgModule({
	declarations: [PeopleListComponent,
    ItemListComponent],
	imports: [],
	exports: [PeopleListComponent,
    ItemListComponent]
})
export class ComponentsModule {}
