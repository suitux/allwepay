import { TranslateService } from '@ngx-translate/core';
import { WhatHavePeopleToPayPage } from './../what-have-people-to-pay/what-have-people-to-pay';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

/**
 * Generated class for the ItemPeopleRelationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-item-people-relation',
  templateUrl: 'item-people-relation.html',
})
export class ItemPeopleRelationPage {

  people = [];
  items = [];

  itemsWithPeople = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, private translateService: TranslateService) {
    this.people = navParams.data.people;
    this.items = navParams.data.items;

    this.items.forEach(item => {
      var itemWithPeople = {item: item, people: [], allPeople: false};
      this.itemsWithPeople.push(itemWithPeople);
    });
  }

  selectAllFromItem(itemWithPeople) {
    
    itemWithPeople.people = [];

    if(itemWithPeople.allPeople) {
      this.people.forEach(person => {
        itemWithPeople.people.push(person.name);
      });
    }

  }

  isListCorrect() : boolean {
    var isListCorrect : boolean = true;

    this.itemsWithPeople.forEach(itemWithPeople => {
      if(isListCorrect && itemWithPeople.people.length == 0)
        isListCorrect = false;
    });

    return isListCorrect
  }

  nextPage() {

    if(this.isListCorrect())
      this.navCtrl.push(WhatHavePeopleToPayPage, { people : this.people, items: this.items, itemsWithPeople : this.itemsWithPeople }, {animate: true, animation: "transition"});
    else
      this.toastCtrl.create({message: this.translateService.instant('NOTALLITEMSRELATEDWITHPEOPLE'), duration: 2000}).present();

  }

}
