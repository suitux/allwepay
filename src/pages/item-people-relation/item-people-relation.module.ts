import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ItemPeopleRelationPage } from './item-people-relation';

@NgModule({
  declarations: [
    ItemPeopleRelationPage,
  ],
  imports: [
    IonicPageModule.forChild(ItemPeopleRelationPage),
  ],
})
export class ItemPeopleRelationPageModule {}
