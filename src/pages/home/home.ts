import { PeopleListComponent } from './../../components/people-list/people-list';
import { Component, ViewChild } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { ItemsPage } from '../items/items';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  @ViewChild('people') peopleList: PeopleListComponent;

  constructor(public navCtrl: NavController, public toastCtrl: ToastController, private translateService: TranslateService) {

  }

  ionViewDidLoad(){
  }

  addPerson() {
    this.peopleList.addPerson("");
  }

  removePerson(person) {
    this.peopleList.removePerson(person);
  }

  nextPage() {
    if(this.peopleList.isAValidList())
      this.navCtrl.push(ItemsPage, { people : this.peopleList.people }, {animate: true, animation: "transition"});
    else {
      this.toastCtrl.create({message: this.translateService.instant('MINIUMONEPERSON'), duration: 2000}).present();
      this.peopleList.addPerson("");
    }
  }
}
