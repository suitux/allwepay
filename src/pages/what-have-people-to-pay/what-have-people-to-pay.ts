import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the WhatHavePeopleToPayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-what-have-people-to-pay',
  templateUrl: 'what-have-people-to-pay.html',
})
export class WhatHavePeopleToPayPage {

  peopleToPay = [];

  SumOfAllPrices : number = 0;

  itemsWithPeople = []

  items = []

  constructor(public navCtrl: NavController, public navParams: NavParams, private socialSharing: SocialSharing) {
    this.itemsWithPeople = navParams.data.itemsWithPeople;
    this.items = navParams.data.items;

    navParams.data.people.forEach(person => {
      this.peopleToPay.push({name: person.name, toPay: 0});
    });

    this.calculate();
  }

  ionViewDidLoad() {
  }

  share() {

    let msg : string = "";

    this.peopleToPay.forEach(person => {
      msg += person.name + ": " + person.toPay;
      msg += "\n";
    });

    this.socialSharing.share(msg);

  }

  calculate() {
    this.itemsWithPeople.forEach(itemWithPeople => {
      
      var priceToPay : number = Number(itemWithPeople.item.quantity) * Number(itemWithPeople.item.price);
      var pricePerPerson : number = priceToPay / Number(itemWithPeople.people.length);

      this.peopleToPay.forEach(person => {
        
        itemWithPeople.people.forEach(relatedPerson => {
          if(person.name == relatedPerson) {
            person.toPay += pricePerPerson;
          }
        });
      });
    });

    this.peopleToPay.forEach(personToPay => {
      this.SumOfAllPrices += Number(personToPay.toPay);
    });
  }

}
