import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WhatHavePeopleToPayPage } from './what-have-people-to-pay';

@NgModule({
  declarations: [
    WhatHavePeopleToPayPage,
  ],
  imports: [
    IonicPageModule.forChild(WhatHavePeopleToPayPage),
  ],
})
export class WhatHavePeopleToPayPageModule {}
