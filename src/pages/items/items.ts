import { TranslateService } from '@ngx-translate/core';
import { ItemPeopleRelationPage } from './../item-people-relation/item-people-relation';
import { ItemListComponent } from './../../components/item-list/item-list';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events, Alert, AlertButton, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-items',
  templateUrl: 'items.html',
})
export class ItemsPage {

  @ViewChild('items') itemsList: ItemListComponent;

  itemToEdit : string = "";

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public alertCtrl: AlertController,
              public events: Events, 
              public toastCtrl: ToastController, 
              private translateService: TranslateService) {
  }

  ionViewDidLeave(){
    this.saveItemsOnCache();
    this.events.unsubscribe('item:click');
  }

  ionViewDidEnter(){
    this.events.subscribe('item:click', (item) => {
      this.promptEditItem(item);
    });
  }

  ionViewDidLoad(){
    if(window.sessionStorage.getItem("itemsList") !== null) {
      this.itemsList.items = JSON.parse(window.sessionStorage.getItem("itemsList"));
    }
  }

  promptAddItem() {
    let itemPrompt : Alert = this.getItemPrompt("", 0, 0);
    itemPrompt.addButton(this.saveButton);
    itemPrompt.addButton(this.nextButton);

    itemPrompt.present();
  }

  promptEditItem(item) {
    this.itemToEdit = item.name;

    let itemPrompt : Alert = this.getItemPrompt(item.name, item.price, item.quantity);
    itemPrompt.setTitle(this.translateService.instant('EDITITEM'));
    itemPrompt.addButton(this.saveButton);

    itemPrompt.present();
  }

  addItem(name: string, price: number, quantity: number) : boolean {

    var isSuccesful : boolean = false;

    try {
      this.itemsList.addItem(name, price, quantity);
      isSuccesful = true;
    } catch(e) {
      this.toastCtrl.create({message: this.translateService.instant('ITEMALREADYEXISTSRENAMEIT'), duration: 2000}).present();
    }

    return isSuccesful;
  }

  editItem(name: string, newItem) : boolean {

    var isSuccesful : boolean = false;

    try {
      this.itemsList.editItem(name, newItem)
      isSuccesful = true;
    } catch(e) {
      this.toastCtrl.create({message: this.translateService.instant('ITEMALREADYEXISTSRENAMEIT'), duration: 2000}).present();
    }

    return isSuccesful;
  }

  saveItemsOnCache() {
    window.sessionStorage.removeItem("itemsList");
    window.sessionStorage.setItem("itemsList", JSON.stringify(this.itemsList.items));
  }

  isItemValid(item) : boolean {
    var isValidated : boolean = false;

    isValidated = (item.name.length > 0) && (item.price > 0) && (item.quantity > 0);

    return isValidated;
  }

  getItemPrompt(name: string, price: number, quantity: number) : Alert {
    var alertControl : Alert = this.alertCtrl.create({
      title: this.translateService.instant('NEWITEM'),
      cssClass: 'alertInput',
      inputs: [
        {
          name: 'name',
          placeholder: this.translateService.instant('NAME'),
          value: name,
          type: "text"
        },
        {
          name: 'price',
          placeholder: this.translateService.instant('PRICE'),
          value: (price == 0)? "" : price.toString(),
          type: "number"
        },
        {
          name: 'quantity',
          placeholder: this.translateService.instant('QUANTITY'),
          value: (quantity == 0)? "" : quantity.toString(),
          type: "number"
        },
      ],
      buttons: [
        {
          text: this.translateService.instant('CANCEL')
        }
      ]
    });

    return alertControl;
  }

  saveButton : AlertButton = {
    text: this.translateService.instant('SAVE'),
    handler: data => {
      if(!this.isItemValid(data)) {
        this.toastCtrl.create({message: this.translateService.instant('CHECKITEMVALUES'), duration: 2000}).present();
        return false;
      }

      var isSuccesful : boolean = false;
      if(this.itemToEdit == "")
        isSuccesful = this.addItem(data.name, data.price, data.quantity);
      else {
        isSuccesful = this.editItem(this.itemToEdit, data)
        this.itemToEdit = "";
      }

      return isSuccesful;
    }
  };

  nextButton : AlertButton = {
    text: this.translateService.instant('NEXT'),
    handler: data => {
      if(!this.isItemValid(data)) {
        this.toastCtrl.create({message: this.translateService.instant('CHECKITEMVALUES'), duration: 2000}).present();
        return false;
      }

      var isSuccesful : boolean = false;

      isSuccesful = this.addItem(data.name, data.price, data.quantity);

      if(isSuccesful)
        this.promptAddItem();
      else
        return false;
    }
  };

  nextPage() {
    if(this.itemsList.items.length > 0)
      this.navCtrl.push(ItemPeopleRelationPage, { people : this.navParams.data.people, items : this.itemsList.items }, {animate: true, animation: "transition"});
    else {
      this.toastCtrl.create({message: this.translateService.instant('PLEASEGIVEMEMINIUMONEITEM'), duration: 2000}).present();
    }
  }
}
